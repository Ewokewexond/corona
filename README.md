es soll im 14 tage rhythmus immer weitere infos geben.
wer weitere infos und änderungen hat, kann mir diese gerne mitteilen
über sachsen und / oder über meissen. vielen dank im vorraus.


-----

## vom 18.7.bis 31.7.

### Maskenpflicht

- Eine Lockerung bei der Maskenpflicht wurde nicht bekannt gegeben.

### Schule

- Nach den Sommerferien soll in den Schulen in Sachsen wieder in den Regelbetrieb wechseln

### Veranstaltungen/Freizeit

- Ferienlager mit entsprechenden Hygienekonzepten wieder möglich.
- Jahrmärkte und Volksfeste mit genehmigtem Konzepten dürfen mit maximal 1.000 Besuchern stattfinden.
- In Theatern, Kinos, Opern, Kongresszentren, Kirchen, Musikclubs und Zirkussen darf der Mindestabstand 
unterschritten werden, wenn es eine verpflichtende Kontaktverfolgung und ein genehmigtes Hygienekonzept gibt.
- Organisierte Tanzveranstaltungen von Tanzschulen und –vereinen seien ebenfalls wieder möglich
- Sportwettkämpfe mit Publikum bis 1.000 Personen erlaubt
- Wettkämpfe im Breiten- und Vereinssport mit bis zu 50 Besuchern benötigen keine explizite Genehmigung
- Weitere Lockerungen bei Veranstaltungen und Sport-Events stellt die Regierung für die nächste, 
dann ab 1. September geltende Schutzverordnung in Aussicht.

### Corona-Tests an sächsischen Flughäfen

- Reiserückkehrer, die auf den Flughäfen Leipzig Dresden aus dem Ausland ankommen, 
sollen die Möglichkeit zu einem Corona-Tests bekommen



## vom 3.7.bis 17.7.

### Mindestabstand 

- von 1,5 Metern gilt nicht in Kindertageseinrichtungen, in Schulen und bei schulischen Veranstaltungen

### Kontaktbeschränkungen

- private Zusammenkünfte in der eigenen Häuslichkeit ohne Begrenzung der Personenzahl zulässig 
- Zusammenkünfte und Ansammlungen im öffentlichen Raum bleiben weiterhin nur allein 
und mit den Angehörigen des eigenen Hausstandes, in Begleitung der Partnerin oder des Partners, 
mit Personen, für die ein Sorge- oder Umgangsrecht besteht, und mit Angehörigen eines weiteren 
Hausstandes oder mit bis zu zehn weiteren Personen erlaubt

### außerhalb des privaten Bereichs

- Familienfeiern, z.B. in Gaststätten mit bis zu 100 Personen zugelassen 
- Öffnen dürfen zudem Musikclubs mit genehmigtem Hygienekonzept aber ohne Tanz


## vom 15.6. bis 29.6.

- [Neue Sächsische Corona-Quarantäne-Verordnung](https://www.coronavirus.sachsen.de/amtliche-bekanntmachungen.html#a-6980)



## vom 6.6. bis 15.6.

In Sachsen werden ab dem 6. Juni wieder größere Familienfeiern und Busreisen möglich sein. Außerdem sollen Besuche in Alten- und Pflegeheimen erleichtert werden. Gleichzeitig müssen die Gesundheitsämter in jedem Fall die Infektionsketten nachverfolgen können. Die Mundschutzpflicht und die Abstandsregeln bleiben weiterhin bestehen.

Sollte Corona wieder verstärkt ausbrechen, werden die betroffenen Landkreise aktiv. Die Regel lautet: bei 35 Neuinfektionen auf 100 000 Einwohner erfolgen verschärfte Kontrollen erfolgen, ab 50 Fällen gibt es sofort wieder Ausgangsbeschränkungen.

Die wichtigsten Fragen und Antworten haben wir ihnen zusammengefasst:

## Schule

Auf den Abstand von 1,5 Metern in Kitas, Schulen und auf Schulfeiern wird ab dem 06.06. verzichtet. Nicht davon betroffen ist der sogenannte eingeschränkte Regelbetrieb an Schulen. Die Trennung in A- und B-Klassen mit abwechselndem wöchentlichen Unterricht an Oberschulen und Gymnasien bleibt somit. Nach wie vor gibt es an Grundschulen KEINE Schulbesuchspflicht.

Zeugnisübergaben und Abschlussfeiern DÜRFEN unter Einhaltung der Hygieneregeln stattfinden.

Elternabende oder z.B. Gremiensitzungen dürfen mit Zustimmung der Leitung der jeweiligen  Institutionen stattfinden.


## KiTa

Festgelegte Gruppen sind weiter verpflichtend. Größere Gruppen sind jedoch möglich, um Betreuung in Rand- oder Pausenzeiten abzusichern. Die Eltern müssen weiterhin eine tägliche Gesundheitsbescheinigung unterschreiben.

## SAUNEN, SCHWIMMHALLEN U.Ä.

Thermen, Schwimmhallen, Heilbäder und Saunen dürfen wieder öffnen.

## Besuche in Altenheimen oder Pflegeeinrichtungen

Besuche sind unter Auflagen wieder möglich. Besucher sollten sich anmelden und ihre Kontaktdaten hinterlassen. Unter diesen Hygieneregeln dürfen auch Kliniken, Reha-Einrichtungen oder Behinderten-Werkstätten wieder Besucher empfangen.

## WAS IST NOCH GESCHLOSSEN?

Jahrmärkte, Clubs, Diskotheken, Tanzhäuser und auch Bordelle bleiben geschlossen. Auch Sportveranstaltungen mit über 1000 Personen wird es zunächst bis Ende August nicht geben.